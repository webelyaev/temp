from apps import db

from enum import unique
from datetime import datetime
from sqlalchemy_json import NestedMutableJson


class Wallets(db.Model):  # type: ignore

    __tablename__ = "Wallets"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64))
    address = db.Column(db.String(64), index=True)
    private_key = db.Column(db.String(64))
    standard = db.Column(db.String(64))
    profile_id = db.Column(db.Integer, db.ForeignKey("Profiles.id"))

    def __init__(self, **kwargs):
        for property, value in kwargs.items():
            # depending on whether value is an iterable or not, we must
            # unpack it's value (when **kwargs is request.form, some values
            # will be a 1-element list)
            if hasattr(value, "__iter__") and not isinstance(value, str):
                # the ,= unpack of a singleton fails PEP8 (travis flake8 test)
                value = value[0]

            setattr(self, property, value)

    def __repr__(self):
        return str(self.name)

    def to_dict(self):
        return {
            "name": self.name,
            "address": self.address,
            "privateKey": self.private_key,
            "standard": self.standard,
            "profileId": self.profile_id,
        }


class Networks(db.Model):  # type: ignore

    __tablename__ = "Networks"

    id = db.Column(db.Integer, primary_key=True)
    symbol = db.Column(db.String(64))
    name = db.Column(db.String(64), unique=True)
    chain_id = db.Column(db.Integer, unique=True)
    blocktime = db.Column(db.Float)
    is_active = db.Column(db.Boolean, default=True)

    exchanges = db.relationship("Exchanges", backref="Networks", lazy="dynamic")
    tokens = db.relationship("Tokens", backref="Networks", lazy="dynamic")
    pairs = db.relationship("Pairs", backref="Networks", lazy="dynamic")
    # blocks = db.relationship("JobsScanBlocks", backref="Networks", lazy="dynamic")

    data = db.Column(NestedMutableJson)

    def __init__(self, **kwargs):
        for property, value in kwargs.items():
            # depending on whether value is an iterable or not, we must
            # unpack it's value (when **kwargs is request.form, some values
            # will be a 1-element list)
            if hasattr(value, "__iter__") and not isinstance(value, str):
                # the ,= unpack of a singleton fails PEP8 (travis flake8 test)
                value = value[0]

            setattr(self, property, value)

    def __repr__(self):
        return str(self.name)

    def to_dict(self):
        return {
            "symbol": self.symbol,
            "name": self.name,
            "chainId": self.chain_id,
            "blocktime": self.blocktime,
            "exchanges": self.exchanges,
            "tokens": self.tokens,
            "pairs": self.pairs,
            "data": self.data,
        }


class Exchanges(db.Model):  # type: ignore

    __tablename__ = "Exchanges"

    id = db.Column(db.Integer, primary_key=True)
    code = db.Column(db.String(64), unique=True)
    name = db.Column(db.String(64))
    network_id = db.Column(db.Integer, db.ForeignKey("Networks.id"))
    token_id = db.Column(db.Integer, db.ForeignKey("Tokens.id"))
    router_address = db.Column(db.String(64), unique=True)
    factory_address = db.Column(db.String(64), unique=True)
    data = db.Column(NestedMutableJson)

    def __init__(self, **kwargs):
        for property, value in kwargs.items():
            # depending on whether value is an iterable or not, we must
            # unpack it's value (when **kwargs is request.form, some values
            # will be a 1-element list)
            if hasattr(value, "__iter__") and not isinstance(value, str):
                # the ,= unpack of a singleton fails PEP8 (travis flake8 test)
                value = value[0]

            setattr(self, property, value)

    def __repr__(self):
        return str(self.name)

    def to_dict(self):
        return {
            "code": self.symbol,
            "name": self.name,
            "networkId": self.network_id,
            "tokenId": self.token_id,
            "routerAddress": self.router_address,
            "factoryAddress": self.factory_address,
            "data": self.data,
        }


class Tokens(db.Model):  # type: ignore

    __tablename__ = "Tokens"

    id = db.Column(db.Integer, primary_key=True)
    address = db.Column(db.String(64), index=True, unique=True)
    symbol = db.Column(db.String(128))
    name = db.Column(db.String(128))
    network_id = db.Column(db.Integer, db.ForeignKey("Networks.id"))
    exchange_id = db.relationship("Exchanges", backref="Tokens", lazy="dynamic")
    type = db.Column(db.String(64))  # base, stable
    price = db.Column(db.Float)  # to delete
    data = db.Column(NestedMutableJson)

    def __init__(self, **kwargs):
        for property, value in kwargs.items():
            # depending on whether value is an iterable or not, we must
            # unpack it's value (when **kwargs is request.form, some values
            # will be a 1-element list)
            if hasattr(value, "__iter__") and not isinstance(value, str):
                # the ,= unpack of a singleton fails PEP8 (travis flake8 test)
                value = value[0]

            setattr(self, property, value)

    def to_dict(self):
        return {
            "address": self.address,
            "code": self.symbol,
            "name": self.name,
            "networkId": self.network_id,
            "exchangeId": self.exchange_id,
            "type": self.type,
            "price": self.price,
            "data": self.data,
        }


PairsTokens = db.Table(
    "PairsTokens",
    db.Column("pair_id", db.Integer, db.ForeignKey("Pairs.id")),
    db.Column("token_id", db.Integer, db.ForeignKey("Tokens.id")),
)


PairsProfiles = db.Table(
    "PairsProfiles",
    db.Column("pair_id", db.Integer, db.ForeignKey("Pairs.id")),
    db.Column("profile_id", db.Integer, db.ForeignKey("Profiles.id")),
)


class Pairs(db.Model):  # type: ignore

    __tablename__ = "Pairs"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    symbol = db.Column(db.String(128))
    name = db.Column(db.String(128))
    fullname = db.Column(db.String(256))
    address = db.Column(db.String(64), index=True, unique=True)
    is_active = db.Column(db.Boolean, default=True)
    network_id = db.Column(db.Integer, db.ForeignKey("Networks.id"))
    exchange_id = db.Column(db.Integer, db.ForeignKey("Exchanges.id"))
    network = db.relationship("Networks", backref="Pairs", viewonly=True)
    exchange = db.relationship("Exchanges", backref="Pairs", viewonly=True)
    pair_alerts = db.relationship("PairsAlerts", backref="Pairs", lazy="dynamic")

    tokens = db.relationship("Tokens", secondary=PairsTokens, backref="Pairs")
    base_token_id = db.Column(db.Integer, db.ForeignKey("Tokens.id"))
    price_base = db.Column(db.Float)
    price = db.Column(db.Float)
    price_round = db.Column(db.Float)
    price_300 = db.Column(db.Float)
    price_3600 = db.Column(db.Float)
    price_21600 = db.Column(db.Float)
    price_86400 = db.Column(db.Float)
    price_changed = db.Column(db.Boolean, default=False)
    liquidity = db.Column(db.Float)

    def __init__(self, **kwargs):
        for property, value in kwargs.items():
            # depending on whether value is an iterable or not, we must
            # unpack it's value (when **kwargs is request.form, some values
            # will be a 1-element list)
            if hasattr(value, "__iter__") and not isinstance(value, str):
                # the ,= unpack of a singleton fails PEP8 (travis flake8 test)
                value = value[0]

            setattr(self, property, value)

    def to_dict(self):
        return {
            "symbol": self.symbol,
            "name": self.name,
            "address": self.address,
            "isActive": self.address,
            "networkId": self.network_id,
            "exchangeId": self.exchange_id,
            "network": self.network,
            "exchange": self.exchange,
            "pairAlerts": self.pair_alerts,
            "tokens": self.tokens,
            "baseTokenId": self.base_token_id,
            "priceBase": self.price_base,
            "price": self.price,
            "liquidity": self.liquidity,
        }


class PairsPrices(db.Model):  # type: ignore

    __tablename__ = "PairsPrices"

    id = db.Column(db.Integer, primary_key=True)
    address = db.Column(db.String(64), index=True)
    symbol = db.Column(db.String(64))
    price = db.Column(db.Float)
    timestamp = db.Column(db.Float)
    created_at = db.Column(db.String(64))

    def __init__(self, **kwargs):
        for property, value in kwargs.items():
            # depending on whether value is an iterable or not, we must
            # unpack it's value (when **kwargs is request.form, some values
            # will be a 1-element list)
            if hasattr(value, "__iter__") and not isinstance(value, str):
                # the ,= unpack of a singleton fails PEP8 (travis flake8 test)
                value = value[0]

            setattr(self, property, value)

    def __repr__(self):
        return str(self.name)


class PairsAlerts(db.Model):  # type: ignore

    __tablename__ = "PairsAlerts"

    id = db.Column(db.Integer, primary_key=True)
    pair_id = db.Column(db.Integer, db.ForeignKey("Pairs.id"))
    profile_id = db.Column(db.Integer, db.ForeignKey("Profiles.id"))
    type = db.Column(db.String(64))
    price = db.Column(db.Float)
    percent = db.Column(db.Float)
    range = db.Column(db.Float)
    status = db.Column(db.Boolean, default=True)
    created_at = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)

    def __init__(self, **kwargs):
        for property, value in kwargs.items():
            # depending on whether value is an iterable or not, we must
            # unpack it's value (when **kwargs is request.form, some values
            # will be a 1-element list)
            if hasattr(value, "__iter__") and not isinstance(value, str):
                # the ,= unpack of a singleton fails PEP8 (travis flake8 test)
                value = value[0]

            setattr(self, property, value)

    def __repr__(self):
        return str(self.name)


ProfilesNotifications = db.Table(
    "ProfilesNotifications",
    db.Column("profile_id", db.Integer, db.ForeignKey("Profiles.id")),
    db.Column("notification_id", db.Integer, db.ForeignKey("Notifications.id")),
)


class Watchlists(db.Model):  # type: ignore

    __tablename__ = "Watchlists"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64))
    profile_id = db.Column(db.Integer, db.ForeignKey("Profiles.id"))
    data = db.Column(NestedMutableJson)

    def __init__(self, **kwargs):
        for property, value in kwargs.items():
            # depending on whether value is an iterable or not, we must
            # unpack it's value (when **kwargs is request.form, some values
            # will be a 1-element list)
            if hasattr(value, "__iter__") and not isinstance(value, str):
                # the ,= unpack of a singleton fails PEP8 (travis flake8 test)
                value = value[0]

            setattr(self, property, value)

    def __repr__(self):
        return str(self.name)


class Notifications(db.Model):  # type: ignore

    __tablename__ = "Notifications"

    id = db.Column(db.Integer, primary_key=True)
    address = db.Column(db.String(64), index=True)
    symbol = db.Column(db.String(64))
    notificationDelta = db.Column(db.Float)
    notificationOffset = db.Column(db.Integer)
    timestamp = db.Column(db.Float)

    def __init__(self, **kwargs):
        for property, value in kwargs.items():
            # depending on whether value is an iterable or not, we must
            # unpack it's value (when **kwargs is request.form, some values
            # will be a 1-element list)
            if hasattr(value, "__iter__") and not isinstance(value, str):
                # the ,= unpack of a singleton fails PEP8 (travis flake8 test)
                value = value[0]

            setattr(self, property, value)

    def __repr__(self):
        return str(self.name)


class ProfilesLimit(db.Model):  # type: ignore

    __tablename__ = "ProfilesLimit"

    id = db.Column(db.Integer, primary_key=True)
    profile_id = db.Column(db.Integer, index=True)
    name = db.Column(db.String(64))

    data = db.Column(NestedMutableJson)

    def __init__(self, **kwargs):
        for property, value in kwargs.items():
            # depending on whether value is an iterable or not, we must
            # unpack it's value (when **kwargs is request.form, some values
            # will be a 1-element list)
            if hasattr(value, "__iter__") and not isinstance(value, str):
                # the ,= unpack of a singleton fails PEP8 (travis flake8 test)
                value = value[0]

            setattr(self, property, value)

    def __repr__(self):
        return str(self.name)


class Settings(db.Model):  # type: ignore

    __tablename__ = "Settings"

    id = db.Column(db.Integer, primary_key=True)
    profile_id = db.Column(db.Integer, index=True)
    data = db.Column(NestedMutableJson)

    def __init__(self, **kwargs):
        for property, value in kwargs.items():
            # depending on whether value is an iterable or not, we must
            # unpack it's value (when **kwargs is request.form, some values
            # will be a 1-element list)
            if hasattr(value, "__iter__") and not isinstance(value, str):
                # the ,= unpack of a singleton fails PEP8 (travis flake8 test)
                value = value[0]

            setattr(self, property, value)

    def __repr__(self):
        return str(self.name)


class pairs_history(db.Model):  # type: ignore

    __tablename__ = "pairs_history"
    __table_args__ = {"extend_existing": True}

    # id = db.Column(db.Integer, primary_key=True)
    time = db.Column(db.DateTime, nullable=False, default=datetime.utcnow, primary_key=True)
    exchange_id = db.Column(db.Integer, nullable=False)
    exchange_code = db.Column(db.String(64), nullable=False)

    pair_id = db.Column(db.Integer, nullable=False)
    pair_symbol = db.Column(db.String(128), nullable=False)

    price_open = db.Column(db.Float)
    price_high = db.Column(db.Float)
    price_low = db.Column(db.Float)
    price_close = db.Column(db.Float)

    volume_from = db.Column(db.Float)
    volume_to = db.Column(db.Float)

    trades_count = db.Column(db.Float)

    def __init__(self, **kwargs):
        for property, value in kwargs.items():
            # depending on whether value is an iterable or not, we must
            # unpack it's value (when **kwargs is request.form, some values
            # will be a 1-element list)
            if hasattr(value, "__iter__") and not isinstance(value, str):
                # the ,= unpack of a singleton fails PEP8 (travis flake8 test)
                value = value[0]

            setattr(self, property, value)

    def __repr__(self):
        return str(self.name)


db.event.listen(
    pairs_history.__table__,
    "after_create",
    db.DDL(f"SELECT create_hypertable('{pairs_history.__tablename__}', 'time');"),
)


class pairs_transactions(db.Model):  # type: ignore

    __tablename__ = "pairs_transactions"
    __table_args__ = {"extend_existing": True}

    # id = db.Column(db.Integer, primary_key=True)
    time = db.Column(db.DateTime, nullable=False, default=datetime.utcnow, primary_key=True)
    chain_id = db.Column(db.Integer, nullable=False)
    block_id = db.Column(db.Integer, nullable=False)
    tx_hash = db.Column(db.String, nullable=False)
    tx_function = db.Column(db.String, nullable=False)
    tx_function_name = db.Column(db.String, nullable=False)
    tx_from = db.Column(db.String, nullable=False)
    exchange_id = db.Column(db.Integer, nullable=False)
    exchange_code = db.Column(db.String(64), nullable=False)
    pair_id = db.Column(db.Integer, nullable=False)
    pair_address = db.Column(db.String, nullable=False)
    pair_symbol = db.Column(db.String(128), nullable=False)
    tx_type = db.Column(db.String(10), nullable=False)
    price_stable = db.Column(db.Float)
    price_base = db.Column(db.Float)
    asset_from_address = db.Column(db.String, nullable=False)
    asset_from_amount = db.Column(db.Float)
    asset_to_address = db.Column(db.String, nullable=False)
    asset_to_amount = db.Column(db.Float)

    def __init__(self, **kwargs):
        for property, value in kwargs.items():
            # depending on whether value is an iterable or not, we must
            # unpack it's value (when **kwargs is request.form, some values
            # will be a 1-element list)
            if hasattr(value, "__iter__") and not isinstance(value, str):
                # the ,= unpack of a singleton fails PEP8 (travis flake8 test)
                value = value[0]

            setattr(self, property, value)

    def __repr__(self):
        return str(self.name)


db.event.listen(
    pairs_transactions.__table__,
    "after_create",
    db.DDL(f"SELECT create_hypertable('{pairs_transactions.__tablename__}', 'time');"),
)
